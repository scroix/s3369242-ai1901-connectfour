from connectfour.ui import start_game
from connectfour.game import Game
from connectfour.board import Board
from connectfour.agents.computer_player import MonteCarloAgent, RandomAgent
from connectfour.agents.agent_student import StudentAgent

import json
import sys

player_1 = RandomAgent("Player 1")
player_2 = StudentAgent("Player 2")


def run_evaluations(count):
    g = Game(player_1, player_2, 6, 7, True, True)

    player_1_wins = 0
    player_2_wins = 0

    for _ in range(0, count):
        result = json.loads(start_game(g, graphics=False, evaluation=True))
        winner = result["winner_id"]
        if winner == 1:
            player_1_wins += 1
        else:
            player_2_wins += 1

        g.reset()

    win_percent = (player_2_wins / count) * 100
    win_message = "{}%".format(win_percent)
    print(win_message)


if len(sys.argv) > 1:
    try:
        count = int(sys.argv[1])
        if len(sys.argv) > 2:
            if str(sys.argv[2]) == "MonteCarlo":
                player_1 = MonteCarloAgent("Player 1")
        run_evaluations(count)
    except ValueError:
        print("That isn't a value!")
else:
    print("Use an integer to describe how many interations you want to attempt!")
