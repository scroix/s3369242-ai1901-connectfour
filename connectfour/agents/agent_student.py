from connectfour.agents.computer_player import RandomAgent
import random


class StudentAgent(RandomAgent):

    # Row x Column transformations
    NEIGHBOUR_LOCATIONS = {
        "LOCATION_NORTH": [-1, 0],
        "LOCATION_NORTHWEST": [-1, -1],
        "LOCATION_WEST": [0, -1],
        "LOCATION_SOUTHWEST": [1, -1],
        "LOCATION_SOUTH": [1, 0],
        "LOCATION_SOUTHEAST": [1, 1],
        "LOCATION_EAST": [0, 1],
        "LOCATION_NORTHEAST": [-1, 1],
    }

    # Heuristic weights used for evaluation
    IMPORTANCE_LOW = 1
    IMPORTANCE_MEDIUM = 2
    IMPORTANCE_HIGH = 100
    IMPORTANCE_EXTREME = 1000

    def __init__(self, name):
        super().__init__(name)
        self.MaxDepth = 2
        self.opponent_id = None

    def get_move(self, board):
        """
        Args:
            board: An instance of `Board` that is the current state of the board.

        Returns:
            A tuple of two integers, (row, col)
        """

        valid_moves = board.valid_moves()
        vals = []
        moves = []

        for move in valid_moves:
            next_state = board.next_state(self.id, move[1])
            moves.append(move)
            vals.append(self.dfMiniMax(next_state, 1))

        bestMove = moves[vals.index(max(vals))]
        return bestMove

    def dfMiniMax(self, board, depth):
        # Goal return column with maximized scores of all possible next states

        if depth == self.MaxDepth:
            return self.evaluateBoardState(board)

        valid_moves = board.valid_moves()
        vals = []
        moves = []

        for move in valid_moves:
            if depth % 2 == 1:
                next_state = board.next_state(self.id % 2 + 1, move[1])
            else:
                next_state = board.next_state(self.id, move[1])

            moves.append(move)
            vals.append(self.dfMiniMax(next_state, depth + 1))

        if depth % 2 == 1:
            bestVal = min(vals)
        else:
            bestVal = max(vals)

        return bestVal

    def evaluateBoardState(self, board):
        """
        A major evaluator, and minor evaluator, with the potential for more to be added.
        The evaluation functions looks at the current state of the board, and returns a score for it.
            A positive score for the player (StudentAgent).
            A negative score for the opponent.
        """

        score = 0

        ### Minor evaluators ###
        """ Some weight for placing a piece in the middle of the board,
        which is an advantages position. """

        distance_to_center = abs(board.last_move[1] - 3)
        bonus = self.IMPORTANCE_LOW
        bonus -= distance_to_center
        score += bonus

        ### Major evaluators ###
        """ Search for Connect4s and threats across the entire board. """
        score += self.yaef(board)

        return score

    def yaef(self, board):
        """
        Yet Another Evaluation Function.
        Note to self: This time, we're evaluating from _this_ agent's perspective.
        There are a number of measures used, including:
            - two-in-a-rows, XX00
            - counting threats (three-in-a-rows), XXX0
            - wins
        """

        # Traverse the board in multiple directions whilst incrementing evaluators.
        evaluators = {
            "two_in_a_rows": {"player": 0, "opponent": 0},
            "threats": {"player": 0, "opponent": 0},
            "wins": {"player": 0, "opponent": 0},
        }
        direction_of_count = [
            self.count_in_a_row,
            self.count_in_a_col,
            self.count_in_ascending_diagonals,
            self.count_in_descending_diagonals,
        ]
        for direction in direction_of_count:
            direction(board, evaluators)

        # A positive integer for student advantage, a negative for opponent
        score = 0

        total_student = (
            evaluators["threats"]["player"] * self.IMPORTANCE_HIGH
            + evaluators["wins"]["player"] * self.IMPORTANCE_EXTREME
            + evaluators["two_in_a_rows"]["player"] * self.IMPORTANCE_MEDIUM
        )

        total_opponent = (
            evaluators["threats"]["opponent"] * self.IMPORTANCE_HIGH
            + evaluators["wins"]["opponent"] * self.IMPORTANCE_EXTREME
            + evaluators["two_in_a_rows"]["opponent"] * self.IMPORTANCE_MEDIUM
        )

        if total_student > total_opponent:
            score = total_student
        else:
            score = total_opponent * -1

        return score

    def count_in_a_row(self, board, evaluators):
        for row in range(board.height):
            for col in range(0, board.width - 3):

                counter = {"player": 0, "opponent": 0, "available": 0}
                self._traverse_direction(
                    board, counter, row, col, self.NEIGHBOUR_LOCATIONS["LOCATION_EAST"]
                )
                self._evaluate(evaluators, counter)

    def count_in_a_col(self, board, evaluators):
        for col in range(board.width):
            for row in range(
                board.height - 1, 0 + 3 - 1, -1
            ):  # 0 starting row, + 3, 3 moves away, - 1, range() is < not <=

                counter = {"player": 0, "opponent": 0, "available": 0}
                self._traverse_direction(
                    board, counter, row, col, self.NEIGHBOUR_LOCATIONS["LOCATION_NORTH"]
                )
                self._evaluate(evaluators, counter)

    def count_in_ascending_diagonals(self, board, evaluators):
        for col in range(board.width - 3):
            for row in range(board.height - 1, 0 + 3 - 1, -1):

                counter = {"player": 0, "opponent": 0, "available": 0}
                self._traverse_direction(
                    board,
                    counter,
                    row,
                    col,
                    self.NEIGHBOUR_LOCATIONS["LOCATION_NORTHEAST"],
                )
                self._evaluate(evaluators, counter)

    def count_in_descending_diagonals(self, board, evaluators):
        for col in range(board.width - 3):
            for row in range(board.height - 3):

                counter = {"player": 0, "opponent": 0, "available": 0}
                self._traverse_direction(
                    board,
                    counter,
                    row,
                    col,
                    self.NEIGHBOUR_LOCATIONS["LOCATION_SOUTHEAST"],
                )
                self._evaluate(evaluators, counter)

    def _traverse_direction(self, board, counter, row, col, DIRECTION):
        for i in range(0, 4):
            next_row = row + DIRECTION[0] * i
            next_col = col + DIRECTION[1] * i
            cell_owner = board.get_cell_value(next_row, next_col)
            self._update_counter(counter, cell_owner)

    def _update_counter(self, counter, owner):
        if owner == self.id:
            counter["player"] += 1
        elif owner == 0:
            counter["available"] += 1
        else:
            counter["opponent"] += 1

    def _evaluate(self, evaluators, counter):

        # i.e. XX00
        if (counter["player"] == 2) and (counter["available"] == 2):
            evaluators["two_in_a_rows"]["player"] += 1
            return

        if (counter["opponent"] == 2) and (counter["available"] == 2):
            evaluators["two_in_a_rows"]["opponent"] += 1
            return

        # i.e. XXX0
        if (
            (counter["player"] == 3)
            and (counter["opponent"] == 0)
            and (counter["available"] == 1)
        ):
            evaluators["threats"]["player"] += 1
            return

        if (
            (counter["opponent"] == 3)
            and (counter["player"] == 0)
            and (counter["available"] == 1)
        ):
            evaluators["threats"]["opponent"] += 1
            return

        # i.e. XXXX
        if counter["player"] == 4:
            evaluators["wins"]["player"] += 1
            return

        if counter["opponent"] == 4:
            evaluators["wins"]["opponent"] += 1
            return
