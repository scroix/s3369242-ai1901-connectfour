# Submission

1. A      PDF report called <yourstudentnumber>.pdf      containing a description of how your Connect 4 agent works, and its weak and strong points.



## Note

You also need to write a short (1-2 page) report describing your Agent’s inner workings (may include a description of what your evaluation function looks at, or what other AI techniques you added to the agent). You must also describe the agent’s performance, including its strengths and its weaknesses. Your mark in this part will depend on how well your agent plays and your understanding of what you have created: